let
  oxalica_overlay = import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ (self: super: { local = import ./buildtools { pkgs = super; };})  oxalica_overlay ]; };


in with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    (rust-bin.selectLatestNightlyWith (toolchain: toolchain.default.override {
      extensions = [ "rust-src" "llvm-tools-preview"];
      targets = [ "riscv32imc-unknown-none-elf" ];
    }))

    # Add some extra dependencies from `pkgs`
    libgudev.dev
    udev.dev    
    pkg-config 
    stdenv
    local.espflash
    cargo-binutils
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

}


