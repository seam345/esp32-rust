# Esp32 rust

This is a repo of me leaning as much as I can about the ESP32-C3 with rust

## intaial blinky test
Took me a while to setup my environment using nix but eventually got it working and constructed a [shell.nix](./shell.nix).
**Note:** The user needs access to serial devices this can be achieved by adding your user to the `dialout` group.

Assuming you use nic or Nixos you can run the hello_rgb example with the bellow
```bash
nix-shell
cd esp32c3-hal/
cargo espflash --release --monitor --example hello_rgb --features="smartled"
```
- [ ] annoyingly the above didn't work on a friends laptop, however I didn't realise I was committing a target directory that may have been the issue but untested
changing the `--example <example>` will change which examples to run sometimes additional features are not needed

p.s. these examples are copied from https://github.com/esp-rs/esp-hal

turns out hal examples nolonger need nightly so I have made a nix-shell with stable
```bash
nix-shell shell-latest-stable.nix
cd esp32c3-hal/
cargo espflash --release --monitor --example hello_rgb --features="smartled"
```
- [ ] Keep an eye on releases to see if we can move wifi to nightly aswell


## Attempting to get WIFI working
Looks like wifi config is at https://github.com/esp-rs/esp-wifi
### what I did (todos have checkmarks)
- Initial skim of readme
  - [ ] doesn't work in direct-boot mode (no idea what direct boot is)
    - [ ] initial google found this repo to read up on https://github.com/espressif/esp32c3-direct-boot-example
- inside this repo
  - `nix-shell` 
  - cloned repo `git clone https://github.com/esp-rs/esp-wifi.git`
  - `cd esp-wifi`
  - tried `cargo espflash --release --monitor --example dhcp_esp32c3`
    - failed `thread 'main' panicked at 'Select a chip via it's cargo feature', build.rs:147:5`
  - tried `cargo espflash --release --monitor --example dhcp_esp32c3  --features "esp32c3,embedded-svc"`
    - failed 
      - `error: environment variable "SSID" not defined`  `examples/dhcp_esp32c3/main.rs:27:20`
      - `error: environment variable "PASSWORD" not defined` `examples/dhcp_esp32c3/main.rs:28:24`
    - updated the two variables at line 27 and 28
  - retried `cargo espflash --release --monitor --example dhcp_esp32c3  --features "esp32c3,embedded-svc"`
    - woops that was daft they are expecting env variables
  - retried `SSID="<ssid>" PASSWORD="<password>" cargo espflash --release --monitor --example dhcp_esp32c3  --features "esp32c3,embedded-svc"`
    - looking good!



## Getting MQTT working...
I really want this to talk over MQTT as that is the backbone for my home automation

- setup mqtt server on laptop to test `docker run -p 1883:1883 -p 9001:9001 -it  eclipse-mosquitto:2.0 mosquitto -c /mosquitto-no-auth.conf`
- get laptop IP address
- amend esp-wifi/examples/dhcp_esp32c3/main.rs:115 replacing `142, 250, 185, 115` with laptop address
- amend esp-wifi/examples/dhcp_esp32c3/main.rs:116 replacing port `80` with `1183`
- for example changes see `git diff 81d8720^ 81d8720` however you ip will be different
- run
  - Awesome I see connection attempts in the docker logs. However its not valid mqtt messages yet, not to surprising it was a http message
    - 1661519163: New connection from 172.16.102.109:41000 on port 1883.
    - 1661519163: Client <unknown> disconnected due to malformed packet.
- ok now we need to send valid mqtt packets instead of http ones, we could go to a lot of effort and learn MQTT to hand craft them or we cam use a create
  - I added mqttrs this needed adding to the cargo.toml file with `default-features = false` to disable std so full line is `mqttrs = { version = "0.4", default-features = false }`
  - Right so we need to replace this line `.send_slice(&b"GET / HTTP/1.0\r\nHost: www.google.com\r\n\r\n"[..])` well mainly the bytes sent
  - in mqttrs example we see it make a connect packet, this can be just copy pasted :D however sadly the bytes crate wont compile with no_std so an alternative needs to be found to convert it into bytes. https://crates.io/crates/mqttrs
    - however if you opten the documentation you find `encode_slice` is used and we can use this to convert our packet to bytes https://docs.rs/mqttrs/0.4.1/mqttrs/
  - this change was a little bigger than last so again `git diff 353ab63^ 353ab63` will show the correct changes
  - run
    - Neat we see a correct mqtt connection, the esp closes the socket straight away so its not a graceful disconect
      - 1661519195: New connection from 172.16.102.109:41000 on port 1883.
      - 1661519195: New client connected from 172.16.102.109:41000 as doc_client (p2, c1, k30).
      - 1661519195: Client doc_client disconnected due to protocol error.
- Right lets see if we can keep connection open and send a message or subscribe to topic
  - ok everything I tried so far I just see the disconect almost imediatly... this feels wrong when mqtt keep alive is 30 seconds
    - WIRESHARK ok lets get out wireshark to see whats really happening
      - oh no....
      - right so we have esp connects to broker and sends connect mqtt
      - broker sends MWTT CONNACK which can be safely ignored.... However, a TCP ACK needs to be sent back to say we received the TCP packet...
        - I think this is the point MQTT disconnects as TCP is reporting broken pipe due to lack of ACK and thus MQTT is rightfully closing
    - Ok so we need to get TCP to reply with an ACK
    - Right so been a lot of staring at wireshark and tweaking values... but solved it now 
      - turns out for a while I was sending the entire 1024 byte buffer instead of the 24 bytes of the connect packet  adding `[..len]`  to buf fixes that `git diff d32b5a3^ d32b5a3`
      - I could then successfully hold the connection and send MQTT packets `git diff mqtt-publish^ mqtt-publish` which I guess section accomplished, except... I was having trouble reading the CONNACK message from the mqtt broker
        - The problem i was getting when trying to read is it would sit in the read stage until the connection closed and couldn't work out why
        - initially sidestepped this entirely skipping stage 2 (read section) and going straight to publish, this is aloud on the MQTT spec so it works but I should really be able to read messages
        - took me a while to work out what was happening to prevent the read but as this was initially making http calls it so it expects the socket to be closed when all data was sent. So  the read was only moving on after a closed connection or a failed read hence `if let Ok()` so I had to work out when I have successfully read a full packet and move on
        - This was what I came up with `git diff fix-read^ fix-read` waiting for something in the read buffer, then reading until the read buffer is emtpy, I figured if I pass in with read buffer containing something and the last read resulted in an empty buffer I should have everything (I still can't be 100% sure if this is a full packet but works for now)
- Clean up
  - This stage way of doing things is not very useful, I think I need a new model to more easily work on the MQTT stack
  - so I split it up
    - the initial match statement deals with sending and receiving TCP packets
    - the second match statement deals with MQTT message
    - admittedly I did cop out and put the CONNECT packet in the TCP layer, maybe at some point I'll remove that
- Then spent ages attempting to get RGB light to work (should add more notes)
  - However, this failed for ages... so fell back to just toggling the red led and getting the RGB working another day
- Eventually got the RGB working your able to send a number between 0..255 to esp32Led/brightness esp32Led/hue to change hue and brightness at `git tag rgb`
- Getting IMU unit to work
  - Initially googled for variations of `esp32c3 rust imu` and landed on https://github.com/esp-rs/esp-rust-board
    - There was a crate linked in the readme :D and a datasheet, skipping the datasheet for now as I'm not good at reading dense factual things without having used (likely failed) using the thing first
    - so I imported the crate, and opened the docs https://docs.rs/icm42670/0.1.0/icm42670/
      - Oh god this thing doesn't take care of everything for me, looks like i need to communicate wth the device and then send it to this crate for helpful processing https://docs.rs/accelerometer/0.12.0/accelerometer/index.html
      - oh wait a min there is a struct `ICM-42670 driver` maybe i just use this
      - oki adding `use icm42670::Icm42670;` and `let accel = Icm42670::new();` and going with a cargo run gives me some nice info
        - ```txt
          error[E0061]: this function takes 2 arguments but 0 arguments were supplied
             --> examples/dhcp_esp32c3/main.rs:87:17
              |
           87 |     let accel = Icm42670::new();
              |                 ^^^^^^^^^^^^^-- two arguments are missing
              |
           note: associated function defined here
             --> /home/sean/.cargo/registry/src/github.com-1ecc6299db9ec823/icm42670-0.1.0/src/lib.rs:67:12
              |
           67 |     pub fn new(i2c: I2C, address: Address) -> Result<Self, Error<E>> {
              |            ^^^
           help: provide the arguments
              |
           87 |     let accel = Icm42670::new(/* value */, /* Address */);
              |
          ```
        - admittedly I could have looked at the doc but oki now need to get it an I2C and some sorta Address
        - oki in the docs the address is a hyperlink https://docs.rs/icm42670/0.1.0/icm42670/enum.Address.html lets try primary
        - little confused as the readme and board mentions address 0x68
        - oki in the end I had no idea what I was doing but looking into the training material by ferrous-systems https://github.com/ferrous-systems/espressif-trainings/blob/main/advanced/i2c-sensor-reading/examples/part_2.rs we can see how they read the data
          - side note I should really go through that training material! its almost exactly what I'm doing https://github.com/ferrous-systems/espressif-trainings
        - nope that failed they have a lot of different stuff setup (I should learn)
        - looking at the readme for the crate there is an example https://github.com/jessebraham/icm42670-examples/blob/main/esp32c3/Cargo.toml shall try get that to work
          - oki downloaded the example, it expects the crate to be local so I pulled that down too just incase, running it gets `error: linking with 'rust-lld' failed: exit status: 1` no idea what this is so googled it and got here https://github.com/rust-embedded/discovery/issues/246 the second comment mentioned running `cargo update` so I did
            - 1 `cargo update` later and we have lots of compile error not too surprising, probably been a few changes to esp-hal, so I went back to https://github.com/esp-rs/esp-hal and looked at the I2C example to see a large difference in how it got an I2C::new() so made the changes to look as simlar as possible and hey presto it runs :D diff here https://github.com/jessebraham/icm42670-examples/pull/1 however it has now been accepted so you should be able to pull and run
            - Updating this project it still didn't run, now the example was using a local version of the crate whereas crates.io was v0.1.0, so I went to the local version and checked out the v0.1.0 tag to discover it failed, was planning then to just update cargo.toml to checkout the main branch but the maintainer updated the crate so fast after I mentioned the issue I can now just change my cargo.toml to get v0.1.1 and it compiles, thank you Jessebraham https://github.com/jessebraham/icm42670/discussions/4
      - Ok now to bring it all back into the project, ace right I get a single print out on first boot now so it's correctly importing `git diff add-imu-crate^ add-imu-crate`
      - hmmmm actually these numbers seem weird, I have no idea what they are I need 0-90 deg. Thanks to a chat with @pointswaves I would need to convert from forces (what the number are) to angles, googling that landed me [here](https://electronics.stackexchange.com/questions/205658/converting-raw-gyro-l3gd20h-values-into-angles) which explains a little what I need todo and whats happening nicely
        - maybe this crate would solve my problem https://crates.io/crates/datafusion_imu however I wana try the math on the other link first
      - oh actually should have been looking at the accel data again thanks to @pointswaves for correcting my mistake 
        - right accel data is 0 to ±g where g is the set [here](https://docs.rs/icm42670/0.1.1/icm42670/struct.Icm42670.html#method.set_accel_range) as [2,4,8 & 16](https://docs.rs/icm42670/0.1.1/icm42670/enum.AccelRange.html)
        - printing out `println!("{:?}", imu.accel_range().unwrap());` the default range seems to be g16 I only want to calculate the orientation so 2g is enough for me, and may give me more sensitivity 
        - ok sweet lets avg this data over a few loops and send it over mqtt, ideally (i think) I'd like to have it take a value on a roughly fixed time interval but atm dont know how the clocks work so will just go for loops
      - oki lot of tinkering and got it working, however 100 spat out data wayyy to fast so made it 1000 which throws out a value every 1.5~ seconds :D
- Refactoring
  - I have my goal now, I'm gonna use this to hopefully measure the water level on my boat over time. So lets refactor and get rid of as many unwraps as i can I want this code to fail as little as possible
  - Actually to keep this repo as a learning repo for myself I shall move this code to a new project and refactor there
  - Moved to esp32-accelerometer for tighentening up theis use case 
- Reading embedded rust
  - there is a few interesting tools it recomends having installed that would be relvent for this project `cargo-binutils` `GDB`  and `openOCD`
  - cargo-binutils
    - using nix I would much rather have this in my nix-shell so will look at adding the same way i added espflash
    - Darn nix wants a lock file and there isn't one in the github repo https://github.com/rust-embedded/cargo-binutils/tree/v0.3.6
      - maybe if i clone locally and build I can copy the lock file
        - that resulted in rightly the following: clone, add shell.nix for rust, cargo build
      - oki that worked with some tweaking of the shell.nix file however learned i can clone from crates.io instead which didn't need the cargo.lock adding
    - oki the command is there but looks like `llvm-tools-preview` is missing... oh nix.
      - This can be added with `extensions = ["llvm-tools-preview"];` in the top level shell.nix. feels a bit weird to add it in a seperate place 
      - however in my search i discovered there is alread a crate for binutils! https://search.nixos.org/packages?channel=22.05&show=cargo-binutils&from=0&size=50&sort=relevance&type=packages&query=cargo-binutils
      - local version added in `1dd8553` however I'm gonna switch to the upstream version
  - ok awesome we can do something like `MQTT_IP="<IP>>" SSID="<WIFI-SSID>" PASSWORD="<WIFI-PASSWORD>" cargo size --release  --example dhcp_esp32c3  --features "esp32c3,embedded-svc"` to get bin size


- I started a new project on the `emf-tide` branch attempting to rustify the latest [emf badge](https://badge.emfcamp.org/wiki/TiDAL) another esp32 but S3 instead of C3 and best of all with lots of neat accessories e.g. a screen and joystick