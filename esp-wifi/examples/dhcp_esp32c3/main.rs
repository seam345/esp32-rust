#![no_std]
#![no_main]
#![feature(c_variadic)]
#![feature(const_mut_refs)]

use core::str::FromStr;
use crate::alloc::borrow::ToOwned;
use core::fmt::Write;
use accelerometer::vector::F32x3;
use embedded_svc::wifi::{
    AccessPointInfo,
    ClientConfiguration,
    ClientConnectionStatus,
    ClientIpStatus,
    ClientStatus,
    Configuration,
    Status,
    Wifi,
};
use esp32c3_hal::{
    clock::{ClockControl, CpuClock},
    i2c::I2C,
    pac,
    pac::Peripherals,
    prelude::*,
    pulse_control::ClockSource,
    system::SystemExt,
    timer::TimerGroup,
    utils::{smartLedAdapter, SmartLedsAdapter},
    Delay,
    PulseControl,
    Rtc,
    IO,
};
use esp_backtrace as _;
use esp_println::{print, println};
use esp_wifi::{
    create_network_stack_storage,
    network_stack_storage,
    wifi::{initialize, utils::create_network_interface},
    wifi_interface::{timestamp, WifiError},
};
use icm42670::{prelude::*, AccelRange, Address, Icm42670};
use mqttrs::*;
use riscv_rt::entry;
use smart_leds::{
    brightness,
    gamma,
    hsv::{hsv2rgb, Hsv},
    SmartLedsWrite,
};
use smoltcp::{
    iface::SocketHandle,
    socket::{Socket, TcpSocket, TcpState},
    time::Duration,
};

use crate::pac::I2C0;

extern crate alloc;

const SSID: &str = env!("SSID");
const PASSWORD: &str = env!("PASSWORD");
const MQTT_IP: &str = env!("MQTT_IP");
const ASCII_NUMBER_OFFSET: u8 = 48;

#[entry]
fn main() -> ! {
    init_logger();
    esp_wifi::init_heap();

    let mqtt_broker_ip =
        smoltcp::wire::Ipv4Address::from_str(MQTT_IP).expect("Failed to parse mqtt string");

    let mut peripherals = pac::Peripherals::take().unwrap();
    let mut system = peripherals.SYSTEM.split();
    let clocks = ClockControl::configure(system.clock_control, CpuClock::Clock160MHz).freeze();

    let mut rtc = Rtc::new(peripherals.RTC_CNTL);
    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    let mut wdt0 = timer_group0.wdt;
    let timer_group1 = TimerGroup::new(peripherals.TIMG1, &clocks);
    let mut wdt1 = timer_group1.wdt;
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    // Disable watchdogs
    rtc.swd.disable();
    rtc.rwdt.disable();
    wdt0.disable();
    wdt1.disable();

    let i2c = I2C::new(
        peripherals.I2C0,
        io.pins.gpio10,
        io.pins.gpio8,
        400u32.kHz(),
        &mut system.peripheral_clock_control,
        &clocks,
    )
    .unwrap();

    let mut imu = Icm42670::new(i2c, icm42670::Address::Primary).unwrap();

    imu.set_accel_range(AccelRange::G2);
    println!("{:?}", imu.accel_range().unwrap());
    println!("{:?}", imu.accel_norm().unwrap());

    let mut accel_array: [Option<F32x3>; 1000] = [None; 1000]; // using some to make initialising easier (I'm lazy sometimes) but also at some
                                                             // point I'd like to try a fixed interval and set to None of I missed my fixed
                                                             // interval window, oh also it does return a result so dan set error to None
    let mut accel_array_val: usize = 0;
    let mut avg_accel: F32x3 = F32x3::new(0.0, 0.0, 0.0);
    let mut avg_accel_ready = false;
    // let mut avg_accel_sent = false; // not really needed but shall use it for
    // debugging and seeing how many packets I miss

    let mut storage = create_network_stack_storage!(3, 8, 1);
    let ethernet = create_network_interface(network_stack_storage!(storage));
    let mut wifi_interface = esp_wifi::wifi_interface::Wifi::new(ethernet);

    initialize(&mut peripherals.SYSTIMER, peripherals.RNG, &clocks).unwrap();

    println!("{:?}", wifi_interface.get_status());

    println!("Start Wifi Scan");
    let res: Result<(heapless::Vec<AccessPointInfo, 10>, usize), WifiError> =
        wifi_interface.scan_n();
    if let Ok((res, _count)) = res {
        for ap in res {
            println!("{:?}", ap);
        }
    }

    println!("Call wifi_connect");
    let client_config = Configuration::Client(ClientConfiguration {
        ssid: SSID.into(),
        password: PASSWORD.into(),
        ..Default::default()
    });
    let res = wifi_interface.set_configuration(&client_config);
    println!("wifi_connect returned {:?}", res);

    println!("{:?}", wifi_interface.get_capabilities());
    println!("{:?}", wifi_interface.get_status());

    // wait to get connected
    loop {
        if let Status(ClientStatus::Started(_), _) = wifi_interface.get_status() {
            break;
        }
    }
    println!("{:?}", wifi_interface.get_status());

    println!("Start busy loop on main");

    let mut stage = 0;
    let mut idx = 0;
    let mut buffer = [0u8; 8000];
    let mut waiter = 50000;

    let mut mqtt_connected: bool = false;

    let mut http_socket_handle: Option<SocketHandle> = None;

    let connect_packet = Packet::Connect(Connect {
        protocol: Protocol::MQTT311,
        keep_alive: 30,
        client_id: "esp32c3".into(),
        clean_session: true,
        last_will: None,
        username: None,
        password: None,
    });

    // Allocate buffer (should be appropriately-sized or able to grow as needed).
    let mut connect_packet_buf = [0u8; 24];

    // Write bytes corresponding to `&Packet` into the `BytesMut`.
    let connect_packet_len =
        encode_slice(&connect_packet, &mut connect_packet_buf).expect("failed encoding");

    for (handle, socket) in wifi_interface.network_interface().sockets_mut() {
        match socket {
            Socket::Tcp(_) => http_socket_handle = Some(handle),
            _ => {}
        }
    }

    let mut outgoing_mqtt_packet = [0u8; 1024];
    let mut outgoing_mqtt_packet_free = true;
    let mut outgoing_mqtt_packet_len = 0;
    let mut incoming_mqtt_packet = [0u8; 1024];
    let mut incoming_mqtt_packet_free = true;
    let mut incoming_mqtt_packet_len = 0;

    let mut last_sent_subscription_request: u16 = 0;
    let mut subscribed: bool = false;

    // Configure RMT peripheral globally
    let pulse = PulseControl::new(
        peripherals.RMT,
        &mut system.peripheral_clock_control,
        ClockSource::APB,
        0,
        0,
        0,
    )
    .unwrap();

    // We use one of the RMT channels to instantiate a `SmartLedsAdapter` which can
    // be used directly with all `smart_led` implementations
    let mut led = <smartLedAdapter!(1)>::new(pulse.channel0, io.pins.gpio2);

    // Initialize the Delay peripheral, and use it to toggle the LED state in a
    // loop.
    let mut delay = Delay::new(&clocks);

    let mut color = Hsv {
        hue: 0,
        sat: 255,
        val: 255,
    };
    let mut brightness_val = 10;
    let mut data;

    data = [hsv2rgb(color)];
    // When sending to the LED, we do a gamma correction first (see smart_leds
    // documentation for details) and then limit the brightness to 10 out of 255 so
    // that the output it's not too bright.
    led.write(brightness(gamma(data.iter().cloned()), brightness_val))
        .unwrap();

    loop {
        wifi_interface.poll_dhcp().ok();
        wifi_interface.network_interface().poll(timestamp()).ok();

        if let Status(
            ClientStatus::Started(ClientConnectionStatus::Connected(ClientIpStatus::Done(config))),
            _,
        ) = wifi_interface.get_status()
        {
            let (socket, cx) = wifi_interface
                .network_interface()
                .get_socket_and_context::<TcpSocket>(http_socket_handle.unwrap());

            match  socket.state() {
                TcpState::Established => {
                    if !mqtt_connected {
                        if socket.send_slice(&connect_packet_buf[..connect_packet_len]).is_ok() {
                            // stage = 2; // having problems recieving lets just send morw :D  !
                            mqtt_connected = true; // this is very prosumtus i should check fot eh ack
                            println!("Sent CONNECT");
                        }
                    } else {
                        if socket.may_recv() {
                            if incoming_mqtt_packet_free {
                                if socket.recv_queue() > 0 {
                                    if let Ok(s) = socket.recv_slice(&mut incoming_mqtt_packet[incoming_mqtt_packet_len..]) {
                                        if s > 0 {
                                            incoming_mqtt_packet_len += s;
                                        }
                                        if socket.recv_queue() == 0 {
                                            println!("received packet {}", s);
                                            incoming_mqtt_packet_free = false;
                                        }
                                    } else {
                                        println!("Failed read");
                                    }
                                }
                            }
                            if !outgoing_mqtt_packet_free {
                                if let Ok(s) = socket.send_slice(&outgoing_mqtt_packet[..outgoing_mqtt_packet_len]){
                                    if s == outgoing_mqtt_packet_len {
                                        outgoing_mqtt_packet_free = true;
                                        println!("sent packet");
                                    }
                                }else{
                                    println!("Opps this should be handling and will break functionality")
                                }
                            }
                        } else if socket.may_send() {
                            println!("close");
                            socket.close();
                        }
                    }
                },
                TcpState::Closed => {
                    println!("closed");
                    mqtt_connected = false;
                    subscribed = false;
                    println!("My IP config is {:?}", config);
                    println!("Lets connect");
                    let address = mqtt_broker_ip;
                    let remote_endpoint = (address, 1883);
                    socket.connect(cx, remote_endpoint, 41000).unwrap();
                    println!("keepalive {:?}", socket.keep_alive());
                    println!("delay {:?}", socket.ack_delay());
                    println!("nagke {:?}", socket.nagle_enabled());
                    // socket.set_ack_delay(Some(Duration::from_millis(1)));
                    // socket.set_nagle_enabled(true);
                    socket.set_keep_alive(Some(Duration::from_millis(5000)));
                    println!("keepalive {:?}", socket.keep_alive());
                    println!("delay {:?}", socket.ack_delay());
                    println!("nagke {:?}", socket.nagle_enabled());

                    // stage = 1;
                    println!("Established TCP link to MQTT broker");
                },
                _ => {
                    // println!("ffs");
                }
                // Listen => {},
                // SynSent => {},
                // SynReceived => {},
                // FinWait1 => {},
                // FinWait2 => {},
                // CloseWait => {},
                // Closing => {},
                // LastAck => {},
                // TimeWait => {},
            }

            if mqtt_connected {
                if !incoming_mqtt_packet_free {
                    let decoded =
                        decode_slice(&mut incoming_mqtt_packet[..incoming_mqtt_packet_len])
                            .expect("decoded")
                            .expect("something");
                    println!("decoded {:?}", decoded);
                    println!("");
                    incoming_mqtt_packet_len = 0;
                    incoming_mqtt_packet_free = true;

                    match decoded {
                        mqttrs::Packet::Suback(_) => {
                            subscribed = true;
                            println!("received suback");
                        }
                        mqttrs::Packet::Publish(packet) => {
                            if packet.topic_name.contains("brightness") {
                                let mut calc_brightness: u8 = 0;
                                let mut payload_iter = packet.payload.iter();
                                // let mut num: Some(u8) = payload_iter.next();
                                while let Some(num) = payload_iter.next() {
                                    if num.is_ascii_digit() {
                                        calc_brightness =
                                            (calc_brightness * 10) + (num - ASCII_NUMBER_OFFSET);
                                    } else {
                                        break;
                                    }
                                }
                                println!("brightness_val: {}", calc_brightness);
                                brightness_val = calc_brightness;
                                led.write(brightness(gamma(data.iter().cloned()), brightness_val))
                                    .unwrap();
                            } else if packet.topic_name.contains("hue") {
                                let mut hue_val: u8 = 0;
                                let mut payload_iter = packet.payload.iter();
                                // let mut num: Some(u8) = payload_iter.next();
                                while let Some(num) = payload_iter.next() {
                                    if num.is_ascii_digit() {
                                        hue_val = (hue_val * 10) + (num - ASCII_NUMBER_OFFSET);
                                    } else {
                                        break;
                                    }
                                }
                                println!("hue_val: {}", hue_val);

                                color.hue = hue_val;
                                // Convert from the HSV color space (where we can easily transition
                                // from one color to the other) to
                                // the RGB color space that we can
                                // then send to the LED
                                data = [hsv2rgb(color)];
                                // When sending to the LED, we do a gamma correction first (see
                                // smart_leds documentation for details) and
                                // then limit the brightness to 10 out of 255 so
                                // that the output it's not too bright.
                                led.write(brightness(gamma(data.iter().cloned()), brightness_val))
                                    .unwrap();
                            } else {
                                // color.hue = hue;
                                color.hue = 0;
                                // Convert from the HSV color space (where we can easily transition
                                // from one color to the other) to
                                // the RGB color space that we can
                                // then send to the LED
                                data = [hsv2rgb(color)];
                                // When sending to the LED, we do a gamma correction first (see
                                // smart_leds documentation for details) and
                                // then limit the brightness to 10 out of 255 so
                                // that the output it's not too bright.
                                led.write(brightness(gamma(data.iter().cloned()), 10))
                                    .unwrap();
                            }
                        }
                        _ => {}
                    }
                }
                if outgoing_mqtt_packet_free {
                    if !subscribed {
                        // not subscribed
                        if last_sent_subscription_request == 0 {
                            let mut topics: heapless::Vec<SubscribeTopic, 5> = heapless::Vec::new();
                            topics.push(SubscribeTopic {
                                topic_path: heapless::String::from("esp32Led/+"),
                                qos: mqttrs::QoS::AtMostOnce,
                            });

                            let mut pkt = Packet::Subscribe(Subscribe {
                                pid: Pid::new(),
                                topics,
                            });

                            outgoing_mqtt_packet_len =
                                encode_slice(&pkt, &mut outgoing_mqtt_packet)
                                    .expect("failed encoding");

                            outgoing_mqtt_packet_free = false;
                            last_sent_subscription_request = 65535;
                            println!("MQTT: made subscription request \"esp32Led/+\"");
                        } else {
                            last_sent_subscription_request -= 1;
                        }
                    } else if avg_accel_ready {
                        // mqttrs::Publish
                        let mut payload = alloc::string::String::new();
                        write!(
                            &mut payload,
                            "{{\"X\":{},\"Y\":{},\"Z\":{}}}",
                            avg_accel.x, avg_accel.y, avg_accel.z
                        );
                        let idk = payload.into_bytes();
                        let mut pkt = Packet::Publish(mqttrs::Publish {
                            dup: false,
                            qospid: mqttrs::QosPid::AtMostOnce,
                            retain: false,
                            topic_name: "esp32/accel",
                            payload: &idk,

                        });

                        outgoing_mqtt_packet_len =
                            encode_slice(&pkt, &mut outgoing_mqtt_packet).expect("failed encoding");

                        outgoing_mqtt_packet_free = false;
                        avg_accel_ready = false;
                        println!("MQTT: sent accel data");
                    }
                }
            }
        }

        if accel_array_val < accel_array.len() {
            match imu.accel_norm() {
                Ok(accel_val) => {
                    accel_array[accel_array_val] = Some(accel_val);
                }
                Err(err) => {
                    println!("{:?}", err);
                }
            }
            accel_array_val += 1;
        } else {
            if avg_accel_ready {
                println!("Shoot failed to send avg_accel: {:?}", avg_accel);
            }
            avg_accel = F32x3::new(0.0, 0.0, 0.0);
            let mut count = 0.0;
            for f32x3_option in accel_array.iter() {
                match f32x3_option {
                    Some(f32x3) => {
                        avg_accel.x += f32x3.x;
                        avg_accel.y += f32x3.y;
                        avg_accel.z += f32x3.z;
                        count += 1.0;
                    }
                    None => {}
                }
            }
            if count > 0.0 {
                avg_accel.x = avg_accel.x / count;
                avg_accel.y = avg_accel.y / count;
                avg_accel.z = avg_accel.z / count;
                avg_accel_ready = true;
            } else {
                println!("Oh no something went wrong with accelerometer!");
            }
            accel_array_val = 0;
        }
    }
}

pub fn init_logger() {
    unsafe {
        log::set_logger_racy(&LOGGER).unwrap();
        log::set_max_level(log::LevelFilter::Info);
    }
}

static LOGGER: SimpleLogger = SimpleLogger;
struct SimpleLogger;

impl log::Log for SimpleLogger {
    fn enabled(&self, _metadata: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        println!("{} - {}", record.level(), record.args());
    }

    fn flush(&self) {}
}
